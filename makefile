.PHONY: clean

all:
	mkdir temp bin
	vivado -mode batch -source example/zedboard_ps7/script.tcl
	cp /temp/project_Example_HwLz77/Example_HwLz77.runs/impl_1/BlockDesignExample_ps7_wrapper.bit bin/bitstream.bit

clean:
	rm -r temp bin
