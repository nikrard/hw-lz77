# Hw-LZ77

Verilog description of a LZ77 IP, that compress a datastream following the LZ77 Algorithm
The folder IP contains the ip already packet, just add this folder to the user ip on vivado and you can use the ip on projects.

This ip works with a DMA or FIFO that suppor AXI stream.
It is advised to use a small fifo as a buffer for the IP output to avoid start up errors.

Folder example contains the application of this IP for the following boards:
	zedboard
	

run the script.tcl on vivado in order to compile the example and generate a bitstream
in order to the script work, the vivado current directory has to be the the main folder of the repository.


#Application example


The IP described here should be connected to a Process System as is presented bellow:

![Scheme](.pic/example1_design.png)

#LZ77 
The LZ77 Algorithm is based on the concept of a 'sliding window' which brings
significant improvements in compression ratio.
 The base concept of this algorithm is a dictionary
based on pointers to represent repeated strings in a byte sequence. The pointer
is formed by the following three parts:

 - offset: that points out how far, from the start of the file, a given string is at
 - length: that tells how many characters past the offset are part of the string
 - The deviating character: an indication that a new string was found


The string is equal to the string from *offset* to *offset+length* plus
the deviating character.

The dictionary used changes dynamically based on the sliding window as the file
is parsed for. The larger the sliding window, the larger the entries in the
dictionary will be.  Given an input "xyyxzxyyx", the output would be like
"xyy(0,1,'z')(0,3,'x')" as explained in Table bellow:


 Position | Symbol | Output 
 ---------|--------|--------
0 | x | x
2 | y | y
3 | x | (0,1,'z')
4 | z | 
5 | x | (0,3,'x')
6 | y | 
7 | y | 
9 | x | 

While this example the output is slightly larger than the input, the method
generally achieves a considerably smaller result given longer input data.


![Scheme](.pic/LZ77Search.png)


The LZ77 algorithm  method has the following logic: it searches, in the 
sliding windows, for the same value as the one on its current position ( blue area). This search is backward ("value search direction" arrow), it starts from the values 
closer to the current position. When a value is found it checks the 
following bytes ("match search direction" arrow), 
comparing them to the bytes following the current position (blue area), registering only the match with bigger length.

The algorithm after checking all previous bytes for a sequence, it writes 
the pointer for the largest sequence found, and moves for the next byte 
after the sequence. This next byte on the example of the figure 
 is the third byte 'B' outside, after the current byte and
outside the sequence. If the search doesn't finds a sequence it writes the 
current byte and moves to the following byte.

In the Deflate specification, the pointer is made up by the distance (
offset) and the length only. The encoded data is made up by literals (the 
literals refers to the representation of symbols on the LZ77 output, will 
be explained ahead ) and a pair of  offset&length.

The smaller division that LZ77 uses for the symbols is the byte, meaning 
that a symbols is a decimal values between 0 and 255. The LZ77 algorithm uses 
values between 0 to 285 to represent the pointers, and the literals.

The output of LZ77 is a mix of pointers and literals. The literal 
represent the literally the symbol in the output datastream, 
having the values from  0 to 255, meaning no change has been made to 
the  symbols binary form,(that is why its called literal).

The remaining values 257 to 285 are used to represent the offset and
the length. For these representations extra bits are needed, in order
to save the pointer correctly, since the length can have values between 3 to
258 and the distance can be between 1 to 32,768.

The value 256 is reserved, this value is used to mark the end of the block, 
it means the end of the LZ77's  datastream. 
