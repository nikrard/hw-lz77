`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.03.2019 17:30:38
// Design Name: 
// Module Name: lz77_pointerdata_mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lz77_pointerdata_mux
    #(parameter WINDOWS_SIZE = 8, WORD_SIZE = 8, TOP =1)
    (
    input clk,
    input rst,
    input EN,
    input [WINDOWS_SIZE:1] bus_ptrv,
    input [WINDOWS_SIZE*16-1:0] bus_offset,
    output [15:0] out_offset,
    output pointer
    
    );
    
    genvar N,i;
    generate
    if (WINDOWS_SIZE>1)
        begin
            wire [15:0] sela_offset;
            wire sela_pointer;
            lz77_pointerdata_mux #(WINDOWS_SIZE/2,WORD_SIZE,0) sela (
            .clk(clk),
            .rst(rst),
            .EN(EN),
            .bus_ptrv(bus_ptrv[WINDOWS_SIZE/2:1]),
            .bus_offset(bus_offset[(WINDOWS_SIZE/2)*16-1:0]),
            .out_offset(sela_offset),
            .pointer(sela_pointer)
            );
            
            wire [15:0] selb_offset;
            wire selb_pointer;
            lz77_pointerdata_mux #(WINDOWS_SIZE/2,WORD_SIZE,0) selb (
            .clk(clk),
            .rst(rst),
            .EN(EN),
            .bus_ptrv(bus_ptrv[WINDOWS_SIZE:WINDOWS_SIZE/2+1]),
            .bus_offset(bus_offset[(WINDOWS_SIZE)*16-1:(WINDOWS_SIZE/2)*16]),
            .out_offset(selb_offset),
            .pointer(selb_pointer)
            );
            
            wire [15:0]w_offset;
            wire w_pointer;
            
            assign w_offset = sela_pointer ?  sela_offset : (selb_pointer ? selb_offset : sela_offset);
            assign w_pointer =  sela_pointer || selb_pointer;
            reg [15:0]r_offset;
            reg r_pointer;
            always @( posedge clk or posedge rst)
                begin
                    if (rst) r_offset <= 0;
                    else if (EN) r_offset <= w_offset;
                    if (rst) r_pointer <= 0;
                    else if (EN) r_pointer <= w_pointer;
                end
           
            assign out_offset   = r_offset;
            assign pointer      = r_pointer;
        end
    else
        begin
//            reg [15:0]r_offset;
//            reg r_end;
//            always @( posedge clk or posedge rst)
//                begin
//                    if (rst)begin
//                        r_offset <= 0;
//                        r_end <= 0;
//                    end
//                    else if (EN) begin
//                        r_offset <= bus_offset;
//                        r_end <= bus_ptrv;
//                        end
//                end
           
            assign out_offset   = bus_offset;
            assign pointer      = bus_ptrv;
        end
    endgenerate
    
    
    
    
endmodule
