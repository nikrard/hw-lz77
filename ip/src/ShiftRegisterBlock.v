`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.03.2019 19:21:37
// Design Name: 
// Module Name: ShiftRegisterBlock
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ShiftRegisterBlock
 #(parameter SR_SIZE = 8, WORD_SIZE = 8)
    (
    input reset,
    input clk,
    input EN,
    input [WORD_SIZE-1:0] in_value,
    output [SR_SIZE*WORD_SIZE-1:0] outbus
    );
    
    reg [WORD_SIZE*(SR_SIZE)-1:0] Q;
    
    genvar i;
    generate
    for(i=0; i<SR_SIZE; i=i+1)
    begin
        always @(posedge clk or posedge reset)
        begin
            if (reset) Q[(i+1)*WORD_SIZE-1:i*WORD_SIZE] <= 0;
            else if(i==0) begin
                if (EN)
                    Q[(i+1)*WORD_SIZE-1:i*WORD_SIZE] <= in_value;
                end
            else begin
                if (EN)
                    Q[(i+1)*WORD_SIZE-1:i*WORD_SIZE] <= Q[i*WORD_SIZE-1:(i-1)*WORD_SIZE]; // Move previous register to the current one
                end
        end
    end
    endgenerate
    
    assign outbus = Q[(SR_SIZE)*WORD_SIZE-1:0];
endmodule
