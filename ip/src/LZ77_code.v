`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.04.2019 16:48:05
// Design Name: 
// Module Name: LZ77_code
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module LZ77_code
#(parameter BITWORDSIZE = 32,OFFSET=16,LENGTH=10,LITERAL=8)
(
    input clk,
    input rst,
    input EN,
    
    input [LENGTH-1:0]in_length,
    input [OFFSET-1:0]in_offset,
    input [LITERAL-1:0]in_literal,
    input in_v,
    input in_type,
    input in_tlast,
    
    output out_v,
    output out_type,
    output out_tlast,
    output [LITERAL-1:0] out_literal,
    output [BITWORDSIZE-1:0] out_len,
    output [BITWORDSIZE-1:0] out_elen,
    output [BITWORDSIZE-1:0]out_dis,
    output [BITWORDSIZE-1:0]out_edis
    );
    
    //code pointer information
    
    reg [LITERAL-1:0] r_out_literal;
    reg r_out_v;
    reg r_out_type;
    reg r_out_tlast;
    always @(posedge clk or posedge rst) begin
        if (rst)begin
            r_out_literal <=0;
            r_out_v<=0;
            r_out_type<=0;
            r_out_tlast<=0;
            end
        else if (EN) begin
            r_out_literal <= in_literal;
            r_out_v <= in_v;
            r_out_type <= in_type;
            r_out_tlast<= in_tlast;
            end
        end
    LZ77_PointerCode Block_PointerCode
    (
    .clk(clk),
    .rst(rst),
    .EN(EN),
    .length(in_length),
    .distance(in_offset),
    .length_code(out_len),
    .extra_length(out_elen),
    .dist_code(out_dis),
    .extra_dist(out_edis)
    );
    
    assign out_literal = r_out_literal;
    assign out_v = r_out_v;
    assign out_type = r_out_type;
    assign out_tlast = r_out_tlast;
endmodule
