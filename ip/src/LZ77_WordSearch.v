`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.04.2019 17:39:21
// Design Name: 
// Module Name: LZ77_WordSearch
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module LZ77_WordSearch
    #(parameter WINDOWS_SIZE = 8, NUMBER_OF_BYTES = 1)
    (
    input clk,
    input rst,
    input EN,
    input [(3+WINDOWS_SIZE)*NUMBER_OF_BYTES*(8)-1:0]reg_bus, 
    input [(3+WINDOWS_SIZE)*NUMBER_OF_BYTES-1:0]reg_v_bus,
    
    output [WINDOWS_SIZE*16-1:0] bus_offset,
    output [10-1:0] src_length,
    output [WINDOWS_SIZE:1] bus_ptrc,
    
    output idle
    );
    wire maxlen;
    wire w_idle;
    wire [9:0]count;
    reg [9:0]reg_counter;
    reg [9:0]reg_length;
    always @( posedge clk or posedge rst)
    begin
        if (rst||w_idle)begin
            reg_counter<=2;
            end
        else if (EN) begin 
            reg_length<=reg_counter;
            reg_counter <= count;
            end
    end
    assign count = reg_counter + 1;
    assign src_length=reg_length;
    assign maxlen = (259==count);
    
    wire [WINDOWS_SIZE:1] bus_running;
    
    reg ridle;
    genvar i;
    generate
    for(i=1; i<= WINDOWS_SIZE; i=i+1)
    begin: lettermatcher
        wire [15:0] offset;
        wire running;
        wire equal;
        assign lettermatcher[i].offset = i;
         MatcherBlock matcher(
        .clk(clk),
        .rst(rst),
        .EN(EN),
        .idle(ridle),
        .bit_v_x2(reg_v_bus[0]),
        .bit_v_x1(reg_v_bus[1]),
        .bit_v_x0(reg_v_bus[2]),
        .bit_v_k2(reg_v_bus[0+i-1]),
        .bit_v_k1(reg_v_bus[1+i-1]),
        .bit_v_xk(reg_v_bus[2+i-1]),
        .byte_x2(reg_bus[(0+1)*8-1:0*8]),
        .byte_x1(reg_bus[(1+1)*8-1:1*8]),
        .byte_x0(reg_bus[(2+1)*8-1:2*8]),
        .byte_k2(reg_bus[(1+i)*8-1:(1+i-1)*8]),
        .byte_k1(reg_bus[(2+i)*8-1:(2+i-1)*8]),
        .byte_xk(reg_bus[(3+i)*8-1:(3+i-1)*8]),
        .bit_running(lettermatcher[i].running),
        .pointer_calc(bus_ptrc[i])
        );
        
        assign bus_offset[i*16-1:(i-1)*16] = lettermatcher[i].offset;
        assign bus_running[i] = lettermatcher[i].running;
        //assign bus_running[i] = lettermatcher[i].equal;
    end
    endgenerate
    
    assign w_idle =(bus_running==0)? 1 : maxlen;//improve this part
    always @(posedge clk or posedge rst)begin
         if (rst) ridle <=0;
         else if (EN) ridle<=w_idle;
    end
    assign idle = ridle;
    
    
endmodule
