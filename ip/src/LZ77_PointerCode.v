`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.04.2019 14:49:03
// Design Name: 
// Module Name: LZ77_PointerCode
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module LZ77_PointerCode
    #(parameter DIST_SIZE = 16,LENG_SIZE = 10, WORD_BIT = 32)
    (
    input clk,
    input rst,
    input EN,
    input [LENG_SIZE-1:0] length,
    input [DIST_SIZE-1:0] distance,
    output [WORD_BIT-1:0] length_code,
    output [WORD_BIT-1:0] extra_length,
    output [WORD_BIT-1:0] dist_code,
    output [WORD_BIT-1:0] extra_dist
    );
    
    
    wire [WORD_BIT-1:0] in_len,in_dis;
        assign  in_len[LENG_SIZE-1:0]       = length;
        assign  in_len[WORD_BIT-1:LENG_SIZE]  = 0;
    
        assign  in_dis[DIST_SIZE-1:0]       = distance;
        assign  in_dis[WORD_BIT-1:DIST_SIZE]  = 0;
    
    wire [WORD_BIT-1:0] base_len,base_dis;
    wire [WORD_BIT-1:0] code_len,code_dis;
    wire [WORD_BIT-1:0] extr_len,extr_dis;
        assign extr_len = in_len - base_len;
        assign extr_dis = in_dis - base_dis;
    
    LZ77_DISTCODE array_dis
	(
    .inval(distance),//input [VAL_SIZE:0] inval,
    .arrayval(base_dis[15:0]),//output [VAL_SIZE:0] arrayval,
    .arrayindex(code_dis[4:0])//output [CODE_SIZE:0] arrayindex
    );
    assign base_dis[WORD_BIT-1:16] = 0;
    assign code_dis[WORD_BIT-1:5] = 0;
    
    wire[WORD_BIT-1:0]raw_code_len;
    LZ77_LENGCODE array_len
	(
    .inval(length),//input [VAL_SIZE:0] inval,
    .arrayval(base_len[9:0]),//output [VAL_SIZE:0] arrayval,
    .arrayindex(raw_code_len[4:0])//output [CODE_SIZE:0] arrayindex
    );
    assign raw_code_len[WORD_BIT-1:5]=0;
    assign code_len=raw_code_len+257;
    assign base_len[WORD_BIT-1:10]=0;
    
    
    reg[WORD_BIT-1:0] reg_length_code,reg_extra_length,reg_dist_code,reg_extra_dist;
        always @(posedge clk or posedge rst) begin
            if (rst) begin
                reg_length_code<=0;
                reg_extra_length<=0;
                reg_dist_code<=0;
                reg_extra_dist<=0;
                end
            else if(EN) begin
                reg_length_code<=code_len;
                reg_extra_length<=extr_len;
                reg_dist_code<=code_dis;
                reg_extra_dist<=extr_dis;
                end
            end                
    assign length_code  = reg_length_code;
    assign extra_length = reg_extra_length;
    assign dist_code    = reg_dist_code;
    assign extra_dist   = reg_extra_dist;
//    assign length_code  = code_len;
//    assign extra_length = extr_len;
//    assign dist_code    = code_dis;
//    assign extra_dist   = extr_dis;    
    
    
endmodule
