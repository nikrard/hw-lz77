`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.03.2019 17:17:02
// Design Name: 
// Module Name: Selector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Selector
    #(parameter WINDOWS_SIZE = 8, WORD_SIZE = 8, TOP =1)
    (
    input clk,
    input rst,
    input EN,
    input matching_idle,
    input [WINDOWS_SIZE:1] bus_ptrc,
    input [WINDOWS_SIZE*16-1:0] bus_offset,
    input [10-1:0] src_length,
    input [WORD_SIZE-1:0] in_literal,//literal
    input in_valid,
    input in_tlast,
    output [15:0] out_offset,
    output [9:0] out_length,
    output [WORD_SIZE-1:0] out_literal,
    output out_valid,
    output pointer,
    output out_tlast
    
    );
    
    wire [WINDOWS_SIZE:1]bus_ptrv;
    assign bus_ptrv = (matching_idle)? bus_ptrc : 0;
    lz77_pointerdata_mux #(WINDOWS_SIZE,WORD_SIZE,1) selptr (
    .clk(clk),
    .rst(rst),
    .EN(EN),
    .bus_ptrv(bus_ptrv[WINDOWS_SIZE:1]),
    .bus_offset(bus_offset[(WINDOWS_SIZE)*16-1:0]),
    .out_offset(out_offset),
    .pointer(pointer)
    );
    
    wire in_letter_v,out_letter_v,out_letter_tl;
    assign in_letter_v = matching_idle && in_valid;
    Sel_wait #(WINDOWS_SIZE,WORD_SIZE) q_literal(
    .clk(clk),
    .rst(rst),
    .EN(EN),
    .idata(in_literal),
    .odata(out_literal)
    );
    Sel_wait #(WINDOWS_SIZE,1) q_letterv(
    .clk(clk),
    .rst(rst),
    .EN(EN),
    .idata(in_letter_v),
    .odata(out_letter_v)
    );
    Sel_wait #(WINDOWS_SIZE,1) q_lettertl(
    .clk(clk),
    .rst(rst),
    .EN(EN),
    .idata(in_tlast),
    .odata(out_letter_tl)
    );
    Sel_wait #(WINDOWS_SIZE,10) q_length(
    .clk(clk),
    .rst(rst),
    .EN(EN),
    .idata(src_length),
    .odata(out_length)
    );
    assign out_valid = out_letter_v;
    assign out_tlast = out_letter_tl;
endmodule
