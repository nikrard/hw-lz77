`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.03.2019 17:13:59
// Design Name: 
// Module Name: Sel_wait
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sel_wait
    #(parameter WINDOWS_SIZE = 8, WORD_SIZE = 8)
    (
        input clk,
        input rst,
        input EN,
        input [WORD_SIZE-1:0] idata,
        output [WORD_SIZE-1:0] odata
    );
    generate
    if (WINDOWS_SIZE>1)
        begin
            
            wire [WORD_SIZE-1:0] w_data;
            Sel_wait #(WINDOWS_SIZE/2,WORD_SIZE) selque (
            .clk(clk),
            .rst(rst),
            .EN(EN),
            .idata(idata),
            .odata(w_data)
            );
            reg [WORD_SIZE-1:0] r_data;
            always @( posedge clk or posedge rst)
                begin
                    if (rst) r_data <= 0;
                    else if (EN) r_data <= w_data;
                end
           
            assign odata   = r_data;
        end
    else
        begin
//            reg [WORD_SIZE-1:0] r_data;
//            always @( posedge clk or posedge rst)
//                begin
//                    if (rst) r_data <= 0;
//                    else if (EN) r_data <= idata;
//                end
            assign odata=idata;
        end
    endgenerate
endmodule
