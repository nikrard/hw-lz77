`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.03.2019 16:04:34
// Design Name: 
// Module Name: MatcherBlock
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MatcherBlock(
    input clk,
    input rst,
    input EN,
    input bit_v_x0,
    input bit_v_x1,
    input bit_v_x2,
    input bit_v_xk,
    input bit_v_k1,
    input bit_v_k2,
    input idle,
    input [7:0] byte_x0,
    input [7:0] byte_x1,
    input [7:0] byte_x2,
    input [7:0] byte_xk,
    input [7:0] byte_k1,
    input [7:0] byte_k2,
    output bit_running,
    //output bit_equal,
    output pointer_calc
    );
    //signal
    reg reg_state;
    wire bit0, bit1,bit2,bit3;
    wire bit4;
    wire bit5;
    //hardware description
    
    //rigth chain
    
    
    //left chain
    
    
    
    //minmatch
//lvl0
wire b_e2,b_e1,b_e0;
wire b_v2,b_v1,b_v0;
wire minMatch,prevrun,match,sequence;
assign b_e0 = (byte_x0 == byte_xk)&& (bit_v_x0 && bit_v_xk);
assign b_e1 = (byte_x1 == byte_k1)&& (bit_v_x1 && bit_v_k1);   //preview-l1
assign b_e2 = (byte_x2 == byte_k2)&& (bit_v_x2 && bit_v_k2);   //preview-l1//preview-l1
//assign b_v0 = bit_v_x0 && bit_v_xk;
//assign b_v1 = bit_v_x1 && bit_v_k1;
//assign b_v2 = bit_v_x2 && bit_v_k2;

reg r_e0,r_e1,r_e2,r_min;
always @( posedge clk or posedge rst)
    begin
        if (rst) begin
            r_e0 <= 0;
            r_e1 <= 0;
            r_e2 <= 0;
            r_min<=0;
        end else if (EN)begin
            r_e0 <= b_e0;
            r_e1 <= b_e1;
            r_e2 <= b_e2;
            r_min<=minMatch;
        end 
    end
//- - - - - - - - - - - - - - - - - -
assign match = r_e0;//(r_e0 && r_v0);
assign minMatch = (r_e1 && r_e2)&& r_e0;// && (r_v1 && r_v2);
assign  sequence = ((r_min && idle) || (prevrun && !idle)) && match;
//lvl 4
    //reg state enable control
    //regs
    reg rptr;
    always @( posedge clk or posedge rst)
    begin
        if (rst)begin reg_state <= 0;rptr<=0;end
        else if (EN)begin reg_state <= sequence;rptr<=prevrun;end
        
    end   
    assign prevrun=reg_state;
    assign bit_running = sequence;
    assign pointer_calc = rptr;
endmodule
