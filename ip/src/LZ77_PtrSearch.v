`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.04.2019 15:48:35
// Design Name: 
// Module Name: LZ77_PtrSearch
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module LZ77_PtrSearch
    #(parameter WINDOWS_SIZE = 8, NUMBER_OF_BYTES = 1)
    (
    input clk,
    input rst,
    input EN,
    input [(3+WINDOWS_SIZE)*NUMBER_OF_BYTES*(8)-1:0]reg_bus, 
    input [(3+WINDOWS_SIZE)*NUMBER_OF_BYTES-1:0]reg_v_bus,
    input in_tlast,
    
    output [15:0] out_offset,
    output [9:0] out_length,
    output [(NUMBER_OF_BYTES*8)-1:0] out_literal,
    output out_valid,
    output out_pointer,
    output out_tlast
    );
    
    wire [WINDOWS_SIZE*16-1:0] bus_offset;
    wire [10-1:0] src_length;
    wire [WINDOWS_SIZE:1] bus_ptrc;
    
    wire idle;
    
    wire [15:0] wout_offset;
    wire [9:0] wout_length;
    wire [(NUMBER_OF_BYTES*8)-1:0] wout_literal;
    wire wout_valid;
    wire wout_pointer;
    wire wout_tlast;
    
    LZ77_WordSearch #(WINDOWS_SIZE,NUMBER_OF_BYTES) Block_WordSearch
    (
        .clk(clk),
        .rst(rst),
        .EN(EN),
        .reg_bus(reg_bus),
        .reg_v_bus(reg_v_bus),
        .bus_offset(bus_offset),
        .src_length(src_length),
        .bus_ptrc(bus_ptrc),
        .idle(idle)
    );
    
    reg [NUMBER_OF_BYTES*8-1:0] r_curr_letter0;
    reg r_curr_letter_v0,r_curr_letter_tl0;
    reg [NUMBER_OF_BYTES*8-1:0] r_curr_letter1;
    reg r_curr_letter_v1,r_curr_letter_tl1;
    reg [NUMBER_OF_BYTES*8-1:0] r_curr_letter;
    reg r_curr_letter_v,r_curr_letter_tl;
    always @(posedge clk or posedge rst)
        begin
            if (rst)begin
                 r_curr_letter_v0 <=0;
                 r_curr_letter_tl0 <=0;
                 r_curr_letter0 <=0;
                 r_curr_letter_v1 <=0;
                 r_curr_letter_tl1 <=0;
                 r_curr_letter1 <=0;
                 r_curr_letter_v <=0;
                 r_curr_letter_tl <=0;
                 r_curr_letter <=0;
                 end
            else if(EN) begin
                r_curr_letter_v0 <= reg_v_bus[2];
                r_curr_letter_tl0 <= in_tlast;
                r_curr_letter0 <= reg_bus[3*NUMBER_OF_BYTES*8-1:2*NUMBER_OF_BYTES*8];
                r_curr_letter_v1 <= r_curr_letter_v0;
                r_curr_letter_tl1 <= r_curr_letter_tl0;
                r_curr_letter1 <= r_curr_letter0;
                r_curr_letter_v <= r_curr_letter_v1;
                r_curr_letter_tl <= r_curr_letter_tl1;
                r_curr_letter <= r_curr_letter1;
                end
        end
    
    //phase two selection
    
    Selector #(WINDOWS_SIZE,NUMBER_OF_BYTES*(8)) Block_PtrSelect
    (
    .clk(clk),
    .rst(rst),
    .EN(EN),
    .matching_idle(idle),
    .bus_ptrc(bus_ptrc),
    .bus_offset(bus_offset),
    .src_length(src_length),
    .in_literal(r_curr_letter),//literal
    .in_valid(r_curr_letter_v),
    .in_tlast(r_curr_letter_tl),
    .out_offset(out_offset),
    .out_length(out_length),
    .out_literal(out_literal),
    .out_valid(wout_valid),
    .pointer(wout_pointer),
    .out_tlast(wout_tlast)
    );
    
   
   assign out_valid = EN? wout_valid : 0 ;
   assign out_pointer = EN? wout_pointer : 0;
   assign out_tlast = EN? wout_tlast : 0;
    
endmodule
