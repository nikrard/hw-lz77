
`timescale 1 ns / 1 ps

	module Hw_Lz77_jecf_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line
        parameter integer LZ77_WINDOW_DEPTH = 64,

		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 8,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 128,
		parameter integer C_M00_AXIS_START_COUNT	= 128
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line

        
		input wire  axis_aclk,
		input wire  axis_reset,
		// Ports of Axi Slave Bus Interface S00_AXIS
		output wire  s00_axis_tready,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		//input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,

		// Ports of Axi Master Bus Interface M00_AXIS
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		//output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready
	);

    Hw_LZ77_block 
    #(LZ77_WINDOW_DEPTH,1,32)
    lz77block
    (
        .clk(axis_aclk),
        .rst(axis_reset),
        .ready(s00_axis_tready),
        .word_in(s00_axis_tdata),
        .bit_in_v(s00_axis_tvalid),
        .bit_in_tlast(s00_axis_tlast),
        .word64_out(m00_axis_tdata),
        .word64v_out(m00_axis_tvalid),
        .word_out_tlast(m00_axis_tlast),
        .word_out_ready(m00_axis_tready)
    );

	endmodule
