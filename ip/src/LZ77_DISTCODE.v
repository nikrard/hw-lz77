`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.04.2019 16:01:54
// Design Name: 
// Module Name: LZ77_DISTCODE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module LZ77_DISTCODE
	#(parameter VAL_SIZE=15,CODE_SIZE=4,MAX_VALUE = 32767 )
	(
    input [VAL_SIZE:0] inval,
    output [VAL_SIZE:0] arrayval,
    output [CODE_SIZE:0] arrayindex
    );
    wire [VAL_SIZE:0] in_val;
    assign in_val = inval-1;
    
    wire [VAL_SIZE:0] bw_av_0 ,bw_av_1 ,bw_av_2 ,bw_av_3 ,bw_av_4 ,bw_av_5 ,bw_av_6 ,bw_av_7 ,bw_av_8 ,bw_av_9 ,bw_av_A ,bw_av_B ,bw_av_C ,bw_av_D ,bw_av_E;
    assign bw_av_0 = (in_val[ 0]) ?     2 :     1;
    assign bw_av_1 = (in_val[ 0]) ?     4 :     3;
    assign bw_av_2 = (in_val[ 1]) ?     7 :     5;
    assign bw_av_3 = (in_val[ 2]) ?    13 :     9;
    assign bw_av_4 = (in_val[ 3]) ?    25 :    17;
    assign bw_av_5 = (in_val[ 4]) ?    49 :    33;
    assign bw_av_6 = (in_val[ 5]) ?    97 :    65;
    assign bw_av_7 = (in_val[ 6]) ?   193 :   129;
    assign bw_av_8 = (in_val[ 7]) ?   385 :   257;
    assign bw_av_9 = (in_val[ 8]) ?   769 :   513;
    assign bw_av_A = (in_val[ 9]) ?  1537 :  1025;
    assign bw_av_B = (in_val[10]) ?  3073 :  2049;
    assign bw_av_C = (in_val[11]) ?  6145 :  4097;
    assign bw_av_D = (in_val[12]) ? 12289 :  8193;
    assign bw_av_E = (in_val[13]) ? 24577 : 16385;
    
    wire [VAL_SIZE:0] w_av_0, w_av_1, w_av_2, w_av_3, w_av_4, w_av_5, w_av_6, w_av_7, w_av_8, w_av_9, w_av_A, w_av_B, w_av_C, w_av_D;
    assign w_av_0 = (in_val[ 1]) ? bw_av_1 : bw_av_0;
    assign w_av_1 = (in_val[ 2]) ? bw_av_2 : w_av_0;
    assign w_av_2 = (in_val[ 3]) ? bw_av_3 : w_av_1;
    assign w_av_3 = (in_val[ 4]) ? bw_av_4 : w_av_2;
    assign w_av_4 = (in_val[ 5]) ? bw_av_5 : w_av_3;
    assign w_av_5 = (in_val[ 6]) ? bw_av_6 : w_av_4;
    assign w_av_6 = (in_val[ 7]) ? bw_av_7 : w_av_5;
    assign w_av_7 = (in_val[ 8]) ? bw_av_8 : w_av_6;
    assign w_av_8 = (in_val[ 9]) ? bw_av_9 : w_av_7;
    assign w_av_9 = (in_val[10]) ? bw_av_A : w_av_8;
    assign w_av_A = (in_val[11]) ? bw_av_B : w_av_9;
    assign w_av_B = (in_val[12]) ? bw_av_C : w_av_A;
    assign w_av_C = (in_val[13]) ? bw_av_D : w_av_B;
    assign w_av_D = (in_val[14]) ? bw_av_E : w_av_C;
    
    
    
    wire [VAL_SIZE:0] bw_ai_0 ,bw_ai_1 ,bw_ai_2 ,bw_ai_3 ,bw_ai_4 ,bw_ai_5 ,bw_ai_6 ,bw_ai_7 ,bw_ai_8 ,bw_ai_9 ,bw_ai_A ,bw_ai_B ,bw_ai_C ,bw_ai_D ,bw_ai_E;
    assign bw_ai_0 = (in_val[ 0]) ?  1 :  0;
    assign bw_ai_1 = (in_val[ 0]) ?  3 :  2;
    assign bw_ai_2 = (in_val[ 1]) ?  5 :  4;
    assign bw_ai_3 = (in_val[ 2]) ?  7 :  6;
    assign bw_ai_4 = (in_val[ 3]) ?  9 :  8;
    assign bw_ai_5 = (in_val[ 4]) ? 11 : 10;
    assign bw_ai_6 = (in_val[ 5]) ? 13 : 12;
    assign bw_ai_7 = (in_val[ 6]) ? 15 : 14;
    assign bw_ai_8 = (in_val[ 7]) ? 17 : 16;
    assign bw_ai_9 = (in_val[ 8]) ? 19 : 18;
    assign bw_ai_A = (in_val[ 9]) ? 21 : 20;
    assign bw_ai_B = (in_val[10]) ? 23 : 22;
    assign bw_ai_C = (in_val[11]) ? 25 : 24;
    assign bw_ai_D = (in_val[12]) ? 27 : 26;
    assign bw_ai_E = (in_val[13]) ? 29 : 28;
    
    wire [VAL_SIZE:0] w_ai_0, w_ai_1, w_ai_2, w_ai_3, w_ai_4, w_ai_5, w_ai_6, w_ai_7, w_ai_8, w_ai_9, w_ai_A, w_ai_B, w_ai_C, w_ai_D;
    assign w_ai_0 = (in_val[ 1]) ? bw_ai_1 : bw_ai_0;
    assign w_ai_1 = (in_val[ 2]) ? bw_ai_2 : w_ai_0;
    assign w_ai_2 = (in_val[ 3]) ? bw_ai_3 : w_ai_1;
    assign w_ai_3 = (in_val[ 4]) ? bw_ai_4 : w_ai_2;
    assign w_ai_4 = (in_val[ 5]) ? bw_ai_5 : w_ai_3;
    assign w_ai_5 = (in_val[ 6]) ? bw_ai_6 : w_ai_4;
    assign w_ai_6 = (in_val[ 7]) ? bw_ai_7 : w_ai_5;
    assign w_ai_7 = (in_val[ 8]) ? bw_ai_8 : w_ai_6;
    assign w_ai_8 = (in_val[ 9]) ? bw_ai_9 : w_ai_7;
    assign w_ai_9 = (in_val[10]) ? bw_ai_A : w_ai_8;
    assign w_ai_A = (in_val[11]) ? bw_ai_B : w_ai_9;
    assign w_ai_B = (in_val[12]) ? bw_ai_C : w_ai_A;
    assign w_ai_C = (in_val[13]) ? bw_ai_D : w_ai_B;
    assign w_ai_D = (in_val[14]) ? bw_ai_E : w_ai_C;
    
    generate
    if (MAX_VALUE >= 24577) begin assign arrayval = w_av_D; assign arrayindex = w_ai_D; end
    else if (MAX_VALUE >= 12289) begin assign arrayval = w_av_C; assign arrayindex = w_ai_C; end
    else if (MAX_VALUE >=  6145) begin assign arrayval = w_av_B; assign arrayindex = w_ai_B; end
    else if (MAX_VALUE >=  3073) begin assign arrayval = w_av_A; assign arrayindex = w_ai_A; end
    else if (MAX_VALUE >=  1537) begin assign arrayval = w_av_9; assign arrayindex = w_ai_9; end
    else if (MAX_VALUE >=   769) begin assign arrayval = w_av_8; assign arrayindex = w_ai_8; end
    else if (MAX_VALUE >=   385) begin assign arrayval = w_av_7; assign arrayindex = w_ai_7; end
    else if (MAX_VALUE >=   193) begin assign arrayval = w_av_6; assign arrayindex = w_ai_6; end
    else if (MAX_VALUE >=    97) begin assign arrayval = w_av_5; assign arrayindex = w_ai_5; end
    else if (MAX_VALUE >=    49) begin assign arrayval = w_av_4; assign arrayindex = w_ai_4; end
    else if (MAX_VALUE >=    25) begin assign arrayval = w_av_3; assign arrayindex = w_ai_3; end
    else if (MAX_VALUE >=    13) begin assign arrayval = w_av_2; assign arrayindex = w_ai_2; end
    else if (MAX_VALUE >=     7) begin assign arrayval = w_av_1; assign arrayindex = w_ai_1; end
    else begin assign arrayval = w_av_0; assign arrayindex = w_ai_0; end
    
    endgenerate
//    genvar i;
//    integer k[0:29]  = {1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577};
//    generate
//        for (i=0; i< VAL_SIZE-1; i=i+1)
//        begin: base_lvl
//            wire [VAL_SIZE:0] bw_av;
//            assign bw_av= (in_val[i]) ? k[i*2+1] :  k[i*2+0];
//        end        
            
//    endgenerate
/*    wire[CODE_SIZE:0] w_0_0;
	wire[CODE_SIZE:0] w_0_1;
	wire[CODE_SIZE:0] w_0_2;
	wire[CODE_SIZE:0] w_0_3;
	wire[CODE_SIZE:0] w_0_4;
	wire[CODE_SIZE:0] w_0_5;
	wire[CODE_SIZE:0] w_0_6;
	wire[CODE_SIZE:0] w_0_7;
	wire[CODE_SIZE:0] w_0_8;
	wire[CODE_SIZE:0] w_0_9;
	wire[CODE_SIZE:0] w_0_A;
	wire[CODE_SIZE:0] w_0_B;
	wire[CODE_SIZE:0] w_0_C;
	wire[CODE_SIZE:0] w_0_D;
	
	wire[CODE_SIZE:0] w_1_0;
	wire[CODE_SIZE:0] w_1_1;
	wire[CODE_SIZE:0] w_1_2;
	wire[CODE_SIZE:0] w_1_3;
	wire[CODE_SIZE:0] w_1_4;
	wire[CODE_SIZE:0] w_1_5;
	wire[CODE_SIZE:0] w_1_6;
	
	wire[CODE_SIZE:0] w_2_0;
	wire[CODE_SIZE:0] w_2_1;
	wire[CODE_SIZE:0] w_2_2;
	wire[CODE_SIZE:0] w_2_3;
	
	wire[CODE_SIZE:0] w_3_0;
	wire[CODE_SIZE:0] w_3_1;
	
	wire[CODE_SIZE:0] w_4_0;
	
	assign w_0_0 = (mid[0]) ? 2 : 1;
	assign w_0_1 = (mid[0]) ? 4 : 3;
	assign w_0_2 = (mid[0]) ? 7 : 5;
	assign w_0_3 = (mid[0]) ? 13 : 9;
	assign w_0_4 = (mid[0]) ? 25 : 17;
	assign w_0_5 = (mid[0]) ? 49 : 33;
	assign w_0_6 = (mid[0]) ? 97 : 65;
	assign w_0_7 = (mid[0]) ? 193 : 129;
	assign w_0_8 = (mid[0]) ? 385 : 257;
	assign w_0_9 = (mid[0]) ? 769 : 513;
	assign w_0_A = (mid[0]) ? 1537 : 1025;
	assign w_0_B = (mid[0]) ? 3073 : 2049;
	assign w_0_C = (mid[0]) ? 6145 : 4097;
	assign w_0_D = (mid[0]) ? 12289 : 8193;
	assign w_0_E = (mid[0]) ? 24577 : 16385;
	
	assign w_1_0 = (mid[1]) ? w_0_1 : w_0_0;
	assign w_1_1 = (mid[1]) ? w_0_3 : w_0_2;
	assign w_1_2 = (mid[1]) ? w_0_5 : w_0_4;
	assign w_1_3 = (mid[1]) ? w_0_7 : w_0_6;
	assign w_1_4 = (mid[1]) ? w_0_9 : w_0_8;
	assign w_1_5 = (mid[1]) ? w_0_B : w_0_A;
	assign w_1_6 = (mid[1]) ? w_0_D : w_0_C;
	
	assign w_2_0 = (mid[2]) ? w_1_1 : w_1_0;
	assign w_2_1 = (mid[2]) ? w_1_3 : w_1_2;
	assign w_2_2 = (mid[2]) ? w_1_5 : w_1_4;
	assign w_2_3 = (mid[2]) ? w_0_E : w_1_6;
	
	assign w_3_0 = (mid[3]) ? w_2_1 : w_2_0;
	assign w_3_1 = (mid[3]) ? w_2_3 : w_2_2;
	
	assign w_4_0 = (mid[4]) ? w_3_1 : w_3_0;
	
	
	assign arrayval = w_4_0;*/
endmodule
