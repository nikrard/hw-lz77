`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.03.2019 19:33:11
// Design Name: 
// Module Name: Hw_LZ77_block
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Hw_LZ77_block
#(parameter WINDOWS_SIZE = 16, NUMBER_OF_BYTES = 1,BITWORDSIZE =32)
(
    input clk,
    input rst,
    input [NUMBER_OF_BYTES*8-1:0] word_in,
    input bit_in_v,
    input bit_in_tlast,
    output [BITWORDSIZE*4-1:0] word64_out,
    output word64v_out,
    output word_out_tlast,
    input word_out_ready,
    
    //output [9:0] out_offset,
    //output [9:0] out_length,
    //output [7:0] out_literal,
    //output out_v,
    //output out_type,
    
    output ready
    );
    wire [(3+WINDOWS_SIZE)*NUMBER_OF_BYTES*(8)-1:0]reg_bus; 
    wire [(3+WINDOWS_SIZE)*NUMBER_OF_BYTES-1:0]reg_v_bus; 
    wire [3-1:0]reg_tl_bus;
    
    wire idle;
    
    wire [WINDOWS_SIZE*16-1:0] bus_offset;
    wire [WINDOWS_SIZE*10-1:0] bus_length;
    wire [WINDOWS_SIZE:1] bus_running;
    wire [WINDOWS_SIZE:1] bus_end;
    
    wire in_valid;
    wire enableAll,enableMatch;
    wire cleanWindows;
    
    //word regs
    //preview+curr+windows
    ShiftRegisterBlock #(3+WINDOWS_SIZE,NUMBER_OF_BYTES*(8)) RegView(
    .reset(rst),
    .clk(clk),
    .EN(enableMatch),
    .in_value(word_in),
    .outbus(reg_bus)
    );
    //valid bit regs
    //preview+curr+windows
    ShiftRegisterBlock #(3+WINDOWS_SIZE,1) RegView_v(
    .reset(rst||cleanWindows),
    .clk(clk),
    .EN(enableMatch),
    .in_value(in_valid),
    .outbus(reg_v_bus)
    );
    //tlast 
    ShiftRegisterBlock #(3,1) RegView_tlast(
    .reset(rst),
    .clk(clk),
    .EN(enableMatch),
    .in_value(bit_in_tlast),
    .outbus(reg_tl_bus)
    );

    //Pointer Search -  search for sequences on the previous data, (window)
    wire [15:0] sel_offset;
    wire [9:0] sel_length;
    wire [7:0] sel_literal;
    wire sel_v;
    wire sel_type;
    wire sel_tlast;
    LZ77_PtrSearch #(WINDOWS_SIZE,NUMBER_OF_BYTES)Block_PtrSearch
    (
    .clk(clk),
    .rst(rst),
    .reg_bus(reg_bus), 
    .reg_v_bus(reg_v_bus),
    .in_tlast(reg_tl_bus[2]),
    .EN(enableMatch),
    
    
    .out_offset(sel_offset),
    .out_length(sel_length),
    .out_literal(sel_literal),
    .out_valid(sel_v),
    .out_pointer(sel_type),
    .out_tlast(sel_tlast)
    );
        //for debug
//        assign out_offset = sel_offset;
//        assign out_length = sel_length;
//        assign out_literal = sel_literal;
//        assign out_v = sel_v;
//        assign out_type = sel_type;
    
    //code pointer information
    wire [BITWORDSIZE-1:0] ptr_len, ptr_elen,ptr_dis,ptr_edis;
    wire [7:0] ptr_lit;
    wire ptr_v,ptr_type,ptr_tlast;
    LZ77_code #(BITWORDSIZE,16,10,8) Block_code
    (
    .clk(clk),
    .rst(rst),
    .EN(enableAll),
    
    .in_length(sel_length),
    .in_offset(sel_offset),
    .in_literal(sel_literal),
    .in_v(sel_v),
    .in_type(sel_type),
    .in_tlast(sel_tlast),
    
    .out_v(ptr_v),
    .out_type(ptr_type),
    .out_tlast(ptr_tlast),
    .out_literal(ptr_lit),
    .out_len(ptr_len),
    .out_elen(ptr_elen),
    .out_dis(ptr_dis),
    .out_edis(ptr_edis)
    );
    
    
    //WriteOutBuffer
    LZ77_DataWriteOutBuffer Block_BufferWriteOut
    (
    .clk(clk),
    .rst(rst),
    .EN(enableAll),
    .flag_valid(ptr_v),
    .flag_pointer(ptr_type),
    .flag_tlast(ptr_tlast),
    .literal(ptr_lit),
    .length_code(ptr_len),
    .extra_length(ptr_elen),
    .dist_code(ptr_dis),
    .extra_dist(ptr_edis),
    .out_word(word64_out),
    .out_valid(word64v_out),
    .out_tlast(word_out_tlast)
    );
    
    genvar seli;
    generate
        wire selector_running[5+$clog2(WINDOWS_SIZE):0];
        for (seli=0; seli<=5+$clog2(WINDOWS_SIZE);seli=seli+1)// 6 fixed lvl of pipeline plus log2(windows) pipeline levels from selecto
        begin:   pipelvl
            reg last_data_on_pipelvl;
            wire still_working;
            always @(posedge clk or posedge rst) begin
                if (rst)
                    last_data_on_pipelvl<=0;
                else if(enableMatch)begin
                    if (seli==0)
                        last_data_on_pipelvl<= bit_in_tlast;
                    else 
                        last_data_on_pipelvl<=pipelvl[seli-1].last_data_on_pipelvl;
                    end
                end
            if (seli==0)
                assign pipelvl[seli].still_working = pipelvl[seli].last_data_on_pipelvl;
            else
                assign pipelvl[seli].still_working = pipelvl[seli-1].still_working||pipelvl[seli].last_data_on_pipelvl;
            assign selector_running[seli]=pipelvl[seli].still_working;
        end 
        wire LZ77HW_working;
        assign LZ77HW_working = selector_running[seli-1];//pipelvl[seli-1].still_working;
    endgenerate
    
    wire lastbreaktime = pipelvl[5].still_working;// || bit_in_tlast;//time between streams
    wire lastinput = LZ77HW_working;
    wire lockInput = ~lastbreaktime;//minimum break between two different streams
    wire hwReady = lockInput && word_out_ready;
    assign cleanWindows = pipelvl[4].last_data_on_pipelvl;//empty ptrSearch windows/cache. makes all data in windows invalid. so it doesnt affect the  next stream compression
    assign in_valid = bit_in_v && hwReady;
    assign ready = hwReady;
    assign enableAll = word_out_ready;
    assign enableMatch = (in_valid || lastinput) && enableAll;
endmodule
