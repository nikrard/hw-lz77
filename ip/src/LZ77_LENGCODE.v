`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.04.2019 16:01:54
// Design Name: 
// Module Name: LZ77_LENGCODE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module LZ77_LENGCODE
	#(parameter VAL_SIZE=9,CODE_SIZE=4)
	(
    input [VAL_SIZE:0] inval,
    output [VAL_SIZE:0] arrayval,
    output [CODE_SIZE:0] arrayindex
    );
    
    wire [VAL_SIZE:0] in_val,last_val;
    assign in_val = inval - 3;// if the minim length signal value is 0 the input value doesnt need to be drcremmented by 3
    assign last_val = 258 - 3;// if the minim length signal value is 0 the value doesnt need to be drcremmented by 3
    
    wire [VAL_SIZE:0] bw_av_0,bw_av_1,bw_av_2,bw_av_3,bw_av_4,bw_av_5,bw_av_6;
    assign bw_av_0 = (in_val[1]) ? ( (in_val[0]) ? (  6) : (  5) ) : ( (in_val[0]) ? (  4) : (  3) );
    assign bw_av_1 = (in_val[1]) ? ( (in_val[0]) ? ( 10) : (  9) ) : ( (in_val[0]) ? (  8) : (  7) );
    assign bw_av_2 = (in_val[2]) ? ( (in_val[1]) ? ( 17) : ( 15) ) : ( (in_val[1]) ? ( 13) : ( 11) );
    assign bw_av_3 = (in_val[3]) ? ( (in_val[2]) ? ( 31) : ( 27) ) : ( (in_val[2]) ? ( 23) : ( 19) );
    assign bw_av_4 = (in_val[4]) ? ( (in_val[3]) ? ( 59) : ( 51) ) : ( (in_val[3]) ? ( 43) : ( 35) );
    assign bw_av_5 = (in_val[5]) ? ( (in_val[4]) ? (115) : ( 99) ) : ( (in_val[4]) ? ( 83) : ( 67) );
    assign bw_av_6 = (in_val[6]) ? ( (in_val[5]) ? (227) : (195) ) : ( (in_val[5]) ? (163) : (131) );
    
    wire [VAL_SIZE:0] w_av_0,w_av_1,w_av_2,w_av_3,w_av_4,w_av_5;
    assign w_av_0 = (in_val[2]) ? bw_av_1 : bw_av_0;
    assign w_av_1 = (in_val[3]) ? bw_av_2 :  w_av_0;
    assign w_av_2 = (in_val[4]) ? bw_av_3 :  w_av_1;
    assign w_av_3 = (in_val[5]) ? bw_av_4 :  w_av_2;
    assign w_av_4 = (in_val[6]) ? bw_av_5 :  w_av_3;
    assign w_av_5 = (in_val[7]) ? bw_av_6 :  w_av_4;
     
    
    wire [CODE_SIZE:0] bw_ai_0,bw_ai_1,bw_ai_2,bw_ai_3,bw_ai_4,bw_ai_5,bw_ai_6;
    assign bw_ai_0 = (in_val[1]) ? ( (in_val[0]) ? ( 3) : ( 2) ) : ( (in_val[0]) ? ( 1) : ( 0) );
    assign bw_ai_1 = (in_val[1]) ? ( (in_val[0]) ? ( 7) : ( 6) ) : ( (in_val[0]) ? ( 5) : ( 4) );
    assign bw_ai_2 = (in_val[2]) ? ( (in_val[1]) ? (11) : (10) ) : ( (in_val[1]) ? ( 9) : ( 8) );
    assign bw_ai_3 = (in_val[3]) ? ( (in_val[2]) ? (15) : (14) ) : ( (in_val[2]) ? (13) : (12) );
    assign bw_ai_4 = (in_val[4]) ? ( (in_val[3]) ? (19) : (18) ) : ( (in_val[3]) ? (17) : (16) );
    assign bw_ai_5 = (in_val[5]) ? ( (in_val[4]) ? (23) : (22) ) : ( (in_val[4]) ? (21) : (20) );
    assign bw_ai_6 = (in_val[6]) ? ( (in_val[5]) ? (27) : (26) ) : ( (in_val[5]) ? (25) : (24) );
    
    wire [CODE_SIZE:0] w_ai_0,w_ai_1,w_ai_2,w_ai_3,w_ai_4,w_ai_5;
    assign w_ai_0 = (in_val[2]) ? bw_ai_1 : bw_ai_0;
    assign w_ai_1 = (in_val[3]) ? bw_ai_2 :  w_ai_0;
    assign w_ai_2 = (in_val[4]) ? bw_ai_3 :  w_ai_1;
    assign w_ai_3 = (in_val[5]) ? bw_ai_4 :  w_ai_2;
    assign w_ai_4 = (in_val[6]) ? bw_ai_5 :  w_ai_3;
    assign w_ai_5 = (in_val[7]) ? bw_ai_6 :  w_ai_4;
     
    
   assign arrayval   = (in_val == last_val)? in_val : w_av_5;
   assign arrayindex = (in_val == last_val)?   28   : w_ai_5;
/*
    // get code and code value based on input value - not working: synt and impl sucess but behaviour incorrect 
    // imple utl [ 47 / 14 / - ]
     reg [VAL_SIZE:0] ival;
     reg [CODE_SIZE:0] index;
    always @*
        begin
          if (inval >= 258)  begin ival <= 258; index <= 28; end
          else if (inval >= 227) begin ival <= 227; index <=  27;end
          else if (inval >= 195) begin ival <= 195; index <=  26;end
          else if (inval >= 163) begin ival <= 163; index <=  25;end
          else if (inval >= 131) begin ival <= 131; index <=  24;end
          else if (inval >= 115) begin ival <= 115; index <=  23;end
          else if (inval >= 99) begin ival <= 99; index <=  22;end
          else if (inval >= 83) begin ival <= 83; index <=  21;end
          else if (inval >= 67) begin ival <= 67; index <=  20;end
          else if (inval >= 59) begin ival <= 59; index <=  19;end
          else if (inval >= 51) begin ival <= 51; index <=  18;end
          else if (inval >= 43) begin ival <= 43; index <=  17;end
          else if (inval >= 35) begin ival <= 35; index <=  16;end
          else if (inval >= 31) begin ival <= 31; index <=  15;end
          else if (inval >= 27) begin ival <= 27; index <=  14;end
          else if (inval >= 23) begin ival <= 23; index <=  13;end
          else if (inval >= 19) begin ival <= 19; index <=  12;end
          else if (inval >= 17) begin ival <= 17; index <=  11;end
          else if (inval >= 15) begin ival <= 15; index <=  10;end
          else if (inval >= 13) begin ival <= 13; index <=  9;end
          else if (inval >= 11) begin ival <= 11; index <=  8;end
          else if (inval >= 10) begin ival <= 10; index <=  7;end
          else if (inval >= 9) begin ival <= 9; index <=  6;end
          else if (inval >= 8) begin ival <= 8; index <=  5;end
          else if (inval >= 7) begin ival <= 7; index <=  4;end
          else if (inval >= 6) begin ival <= 6; index <=  3;end
          else if (inval >= 5) begin ival <= 5; index <=  2;end
          else if (inval >= 4) begin ival <= 4; index <=  1;end
          else if (inval >= 3) begin ival <= 3; index <=  0;end
        end
    assign arrayval = ival; 
    assign arrayindex = index;*/
	
	/*
	// select code value by index
	wire[CODE_SIZE:0] w_0_0;
	wire[CODE_SIZE:0] w_0_1;
	wire[CODE_SIZE:0] w_0_2;
	wire[CODE_SIZE:0] w_0_3;
	wire[CODE_SIZE:0] w_0_4;
	wire[CODE_SIZE:0] w_0_5;
	wire[CODE_SIZE:0] w_0_6;
	wire[CODE_SIZE:0] w_0_7;
	wire[CODE_SIZE:0] w_0_8;
	wire[CODE_SIZE:0] w_0_9;
	wire[CODE_SIZE:0] w_0_A;
	wire[CODE_SIZE:0] w_0_B;
	wire[CODE_SIZE:0] w_0_C;
	wire[CODE_SIZE:0] w_0_D;
	
	wire[CODE_SIZE:0] w_1_0;
	wire[CODE_SIZE:0] w_1_1;
	wire[CODE_SIZE:0] w_1_2;
	wire[CODE_SIZE:0] w_1_3;
	wire[CODE_SIZE:0] w_1_4;
	wire[CODE_SIZE:0] w_1_5;
	wire[CODE_SIZE:0] w_1_6;
	
	wire[CODE_SIZE:0] w_2_0;
	wire[CODE_SIZE:0] w_2_1;
	wire[CODE_SIZE:0] w_2_2;
	wire[CODE_SIZE:0] w_2_3;
	
	wire[CODE_SIZE:0] w_3_0;
	wire[CODE_SIZE:0] w_3_1;
	
	wire[CODE_SIZE:0] w_4_0;
	
	assign w_0_0 = (mid[0]) ? 4 : 3;
	assign w_0_1 = (mid[0]) ? 6 : 5;
	assign w_0_2 = (mid[0]) ? 8 : 7;
	assign w_0_3 = (mid[0]) ? 10 : 9;
	assign w_0_4 = (mid[0]) ? 13 : 11;
	assign w_0_5 = (mid[0]) ? 17 : 15;
	assign w_0_6 = (mid[0]) ? 23 : 19;
	assign w_0_7 = (mid[0]) ? 31 : 27;
	assign w_0_8 = (mid[0]) ? 43 : 35;
	assign w_0_9 = (mid[0]) ? 59 : 51;
	assign w_0_A = (mid[0]) ? 83 : 67;
	assign w_0_B = (mid[0]) ? 115 : 99;
	assign w_0_C = (mid[0]) ? 163 : 131;
	assign w_0_D = (mid[0]) ? 227 : 195;
	
	assign w_1_0 = (mid[1]) ? w_0_1 : w_0_0;
	assign w_1_1 = (mid[1]) ? w_0_3 : w_0_2;
	assign w_1_2 = (mid[1]) ? w_0_5 : w_0_4;
	assign w_1_3 = (mid[1]) ? w_0_7 : w_0_6;
	assign w_1_4 = (mid[1]) ? w_0_9 : w_0_8;
	assign w_1_5 = (mid[1]) ? w_0_B : w_0_A;
	assign w_1_6 = (mid[1]) ? w_0_D : w_0_C;
	
	assign w_2_0 = (mid[2]) ? w_1_1 : w_1_0;
	assign w_2_1 = (mid[2]) ? w_1_3 : w_1_2;
	assign w_2_2 = (mid[2]) ? w_1_5 : w_1_4;
	assign w_2_3 = (mid[2]) ? 258 : w_1_6;
	
	assign w_3_0 = (mid[3]) ? w_2_1 : w_2_0;
	assign w_3_1 = (mid[3]) ? w_2_3 : w_2_2;
	
	assign w_4_0 = (mid[4]) ? w_3_1 : w_3_0;
	
	
	assign arrayval = w_4_0;*/
endmodule
