`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.04.2019 16:17:28
// Design Name: 
// Module Name: LZ77_DataWriteOutBuffer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module LZ77_DataWriteOutBuffer
    #(parameter WORD_BIT = 32,LITERAL_SIZE=8)
    (
    input clk,
    input rst,
    input EN,
    input flag_valid,
    input flag_pointer,
    input flag_tlast,
    input [LITERAL_SIZE-1:0]literal,
    input [WORD_BIT-1:0] length_code,
    input [WORD_BIT-1:0] extra_length,
    input [WORD_BIT-1:0] dist_code,
    input [WORD_BIT-1:0] extra_dist,
    output [(4*WORD_BIT)-1:0]out_word,
    output out_valid,
    output out_tlast
    );
    wire[WORD_BIT-1:0]lit;
    wire[WORD_BIT-1:0] clen;
    wire[WORD_BIT-1:0] elen;
    wire[WORD_BIT-1:0] cdis;
    wire[WORD_BIT-1:0] edis;
    assign lit[LITERAL_SIZE-1:0]=literal;
    assign lit[WORD_BIT-1:LITERAL_SIZE] = 0;//set remaing bits to 0
    
    assign clen[WORD_BIT-1:0]=length_code;
    assign elen[WORD_BIT-1:0]=extra_length;
    assign cdis[WORD_BIT-1:0]=dist_code;
    assign edis[WORD_BIT-1:0]=extra_dist;
    
    //t_last control of buffer
    reg r_tlast,r_flag_forceflush;
    wire forceflush;
     always @(posedge clk or posedge rst) begin
        if (rst) begin
            r_tlast<=0;
            r_flag_forceflush<=0;
            end
        else if(EN) begin
            r_tlast<=flag_tlast;
            r_flag_forceflush<=r_tlast;
            end
        end
    assign out_tlast = r_flag_forceflush;
    assign forceflush = r_flag_forceflush;
    
    //base control for the buffer
    //if 3 or 2 is 1 flush buffer 2 or buffer 1;
    reg [3:0] r_counter;
    wire [3:0] counter;
    assign counter[1:0] =r_counter[1:0];
    assign counter[3:2] = (r_counter[3])? 0 : r_counter[3:2];
    always @(posedge clk or posedge rst) begin
        if (rst) r_counter <=0;
        else if ((flag_valid || forceflush) && EN)begin
            if (flag_pointer)   r_counter<= (forceflush) ? 0 : counter+4;
            else r_counter <= (forceflush) ? 0 : counter+1;
            end
        end
    //buffer
    reg [WORD_BIT-1:0] ra1,ra2,ra3,ra4,rb1,rb2,rb3,rb4;
    always @(posedge clk or posedge rst) begin
        if (rst)begin
            ra1<=0;
            ra2<=0;
            ra3<=0;
            ra4<=0;
            rb1<=0;
            rb2<=0;
            rb3<=0;
            rb4<=0;
            end
        else if (flag_valid && EN)begin
            if (counter==0)begin
                if (flag_pointer)begin
                    ra1<=clen;
                    ra2<=elen;
                    ra3<=cdis;
                    ra4<=edis;
                    rb1<=256;//endcode
                    end
                else begin
                    ra1<=lit;
                    ra2<=256;//endcode
                    end
                end
            else if (counter==1)begin
                if (flag_pointer)begin
                    ra2<=clen;
                    ra3<=elen;
                    ra4<=cdis;
                    rb1<=edis;
                    rb2<=256;//endcode
                    end
                else begin
                    ra2<=lit;
                    ra3<=256;//endcode
                    end
                end
            else if (counter==2)begin
                if (flag_pointer)begin
                    ra3<=clen;
                    ra4<=elen;
                    rb1<=cdis;
                    rb2<=edis;
                    rb3<=256;//endcode
                    end
                else begin
                    ra3<=lit;
                    ra4<=256;//endcode
                    end
                end
            else if (counter==3)begin
                if (flag_pointer)begin
                    ra4<=clen;
                    rb1<=elen;
                    rb2<=cdis;
                    rb3<=edis;
                    rb4<=256;//endcode
                    end
                else begin
                    ra4<=lit;
                    rb1<=256;//endcode
                    end
                end
            else if (counter==4)begin
                if (flag_pointer)begin
                    rb1<=clen;
                    rb2<=elen;
                    rb3<=cdis;
                    rb4<=edis;
                    ra1<=256;//endcode
                    end
                else begin
                    rb1<=lit;
                    rb2<=256;//endcode
                    end
                end
            else if (counter==5)begin
                if (flag_pointer)begin
                    rb2<=clen;
                    rb3<=elen;
                    rb4<=cdis;
                    ra1<=edis;
                    ra2<=256;//endcode
                    end
                else begin
                    rb2<=lit;
                    rb3<=256;//endcode
                    end
                end
            else if (counter==6)begin
                if (flag_pointer)begin
                    rb3<=clen;
                    rb4<=elen;
                    ra1<=cdis;
                    ra2<=edis;
                    ra3<=256;//endcode
                    end
                else begin
                    rb3<=lit;
                    rb4<=256;//endcode
                    end
                end
            else if (counter==7)begin
                if (flag_pointer)begin
                    rb4<=clen;
                    ra1<=elen;
                    ra2<=cdis;
                    ra3<=edis;
                    ra4<=256;//endcode
                    end
                else begin
                    rb4<=lit;
                    ra1<=256;//endcode
                    end
                end
            end
        end
    assign out_word = (counter[2] ^ forceflush) ? {ra4,ra3,ra2,ra1} : {rb4,rb3,rb2,rb1};
    //ctrl of out put
    reg oldbuffer;
    always @(posedge clk or posedge rst) begin
        if (rst) oldbuffer <=0;
        else if(EN) 
            oldbuffer <= (forceflush)? 0 : r_counter[2];
        end
    assign out_valid = (oldbuffer ^ r_counter[2]) ^ forceflush ; //XOR operation
endmodule
