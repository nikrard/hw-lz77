create_project Example_HwLz77 temp/project_Example_HwLz77 -part xc7z020clg484-1

set_property board_part digilentinc.com:zedboard:part0:1.0 [current_project]
set_property  ip_repo_paths  ./ [current_project]
update_ip_catalog

import_files -force -norecurse example/zedboard_ps7/src/BlockDesignExample_ps7.bd
import_files -force -norecurse example/zedboard_ps7/src/BlockDesignExample_ps7_wrapper.v

open_bd_design {temp/project_Example_HwLz77/Example_HwLz77.srcs/sources_1/bd/src/BlockDesignExample_ps7.bd}
#update ip in case of ips been outdated
#report_ip_status -name ip_status 
#upgrade_ip [get_ips  {SystemDesign_axi_smc_0 SystemDesign_processing_system7_0_1 SystemDesign_rst_ps7_0_100M_2}] -log ip_upgrade.log


startgroup
set_property -dict [list CONFIG.LZ77_WINDOW_DEPTH {2048}] [get_bd_cells Hw_LZ77_jecf_0]
endgroup


launch_runs synth_1
launch_runs impl_1 -to_step write_bitstream

wait_on_run impl_1

#file copy -force /temp/project_Example_HwLz77/Example_HwLz77.runs/impl_1/BlockDesignExample_ps7_wrapper.bit bin/bitstream.bit
write_hwdef -force  -file bin/BlockDesignExample_ps7_wrapper.hdf

close_project